package com.github.axet.wget;

import java.net.URL;

import org.json.JSONObject;

import com.github.axet.wget.errors.DownloadIOError;
import com.github.axet.wget.info.DownloadInfo;

public class SerializationTest {

    public static void main(String[] args) {
        try {
            DownloadInfo.Part p = new DownloadInfo.Part();
            p.setStart(1);
            p.setEnd(2);
            p.setIndex(3);
            p.setDelay(4, new DownloadIOError("abc"));
            p.setRetry(5);
            p.setDownloaded(222);
            JSONObject o = p.save();
            System.out.println(o.toString(2));
            p = new DownloadInfo.Part(o);
            o = p.save();
            System.out.println(o.toString(2));
            DownloadInfo info = new DownloadInfo(new URL("https://download.virtualbox.org/virtualbox/6.0.10/VirtualBox-6.0.10-132072-Win.exe"));
            info.extract();
            info.enableMultipart();
            o = info.save();
            System.out.println(o.toString(2));
            info = new DownloadInfo(o);
            o = info.save();
            System.out.println(o.toString(2));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
