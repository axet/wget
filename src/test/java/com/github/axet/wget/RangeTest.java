package com.github.axet.wget;

public class RangeTest {

    public static void main(String[] args) {
        try {
            RangeSet r = new RangeSet();
            for (int i = 0; i < 10; i++)
                r.add(i * 10, (i + 1) * 10 - 1);
            r.exclude(55, 74);
            System.out.println(r);

            r = new RangeSet();
            for (int i = 0; i < 10; i++)
                r.add(i * 10, (i + 1) * 10 - 1);
            r.merge(21, 28);
            System.out.println(r);

            r = new RangeSet();
            for (int i = 0; i < 10; i++)
                r.add(i * 10, (i + 1) * 10 - 1);
            r.exclude(5, 90);
            System.out.println(r);

            r = new RangeSet(0, 99);
            r.exclude(55, 75);
            System.out.println(r);

            r = new RangeSet(0, 99);
            r.exclude(0, 75);
            System.out.println(r);

            r = new RangeSet(0, 99);
            r.exclude(55, 99);
            System.out.println(r);

            r = new RangeSet(0, 99);
            r.exclude(0, 100);
            System.out.println(r);

            r = new RangeSet(0, 99);
            r.exclude(0, 0);
            System.out.println(r);

            r = new RangeSet(55, 75);
            r.exclude(56, 74);
            System.out.println(r);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
