package com.github.axet.wget.info;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * DownloadInfo class. Keep part information. We need to serialize this class between application restart. Thread safe.
 */
public class DownloadInfo extends URLInfo {
    private static final long serialVersionUID = 1952592930771906713L;

    public static long PART_LENGTH = 5 * 1024 * 1024;

    /**
     * part we are going to download.
     */
    protected List<Part> parts;
    protected long partLength;

    /**
     * total bytes downloaded. for chunk download progress info. for one thread count - also local file size;
     */
    protected long downloaded;

    public static class Part {
        /**
         * Notify States
         */
        public enum States {
            QUEUED, DOWNLOADING, RETRYING, ERROR, STOP, DONE;
        }

        /**
         * start offset inclusive [start
         */
        protected long start;
        /**
         * end offset inclusive end]
         */
        protected long end;
        /**
         * part number
         */
        protected long index;
        /**
         * number of bytes we are downloaded
         */
        protected long downloaded;
        /**
         * download state
         */
        protected States state;
        /**
         * downloading error / retry error
         */
        protected Throwable exception;
        /**
         * retrying delay;
         */
        protected int delay;
        /**
         * retry count
         */
        protected int retry;

        public Part() {
        }

        public Part(Part copy) {
            this.start = copy.start;
            this.end = copy.end;
            this.index = copy.index;
            this.downloaded = copy.downloaded;
            this.state = copy.state;
            this.exception = copy.exception;
            this.delay = copy.delay;
            this.retry = copy.retry;
        }

        public Part(JSONObject o) {
            load(o);
        }

        synchronized public long getStart() {
            return start;
        }

        synchronized public void setStart(long start) {
            this.start = start;
        }

        synchronized public long getEnd() {
            return end;
        }

        synchronized public void setEnd(long end) {
            this.end = end;
        }

        synchronized public long getIndex() {
            return index;
        }

        synchronized public void setIndex(long index) {
            this.index = index;
        }

        synchronized public long getLength() {
            return end - start + 1;
        }

        synchronized public long getDownloaded() {
            return downloaded;
        }

        synchronized public void setDownloaded(long count) {
            this.downloaded = count;
        }

        synchronized public States getState() {
            return state;
        }

        synchronized public void setState(States state) {
            this.state = state;
            this.exception = null;
        }

        synchronized public void setState(States state, Throwable e) {
            this.state = state;
            this.exception = e;
        }

        synchronized public Throwable getException() {
            return exception;
        }

        synchronized public void setException(Throwable exception) {
            this.exception = exception;
        }

        synchronized public int getDelay() {
            return delay;
        }

        synchronized public void setDelay(int delay, Throwable e) {
            this.state = States.RETRYING;
            this.delay = delay;
            this.exception = e;
        }

        synchronized public int getRetry() {
            return retry;
        }

        synchronized public void setRetry(int retry) {
            this.retry = retry;
        }

        synchronized public JSONObject save() {
            JSONObject o = new JSONObject();
            o.put("start", start);
            o.put("end", end);
            o.put("index", index);
            o.put("downloaded", downloaded);
            o.put("state", state.toString());
            if (exception != null)
                o.put("exception", DownloadInfo.toString(exception));
            // o.put("delay", delay);
            // o.put("retry", retry);
            return o;
        }

        synchronized public void load(JSONObject o) {
            start = o.getLong("start");
            end = o.getLong("end");
            index = o.getLong("index");
            downloaded = o.getLong("downloaded");
            state = States.valueOf(o.getString("state"));
            String e = o.optString("exception", null);
            if (e != null)
                exception = DownloadInfo.toThroable(e);
            // delay = o.getInt("delay");
            // retry = o.getInt("retry");
        }
    }

    public DownloadInfo(URL source) {
        super(source);
    }

    public DownloadInfo(URL source, ProxyInfo p) {
        super(source);
        setProxy(p);
    }

    public DownloadInfo(JSONObject o) {
        load(o);
    }

    /**
     * is it a multipart download?
     * 
     * @return
     */
    synchronized public boolean multipart() {
        if (!getRange())
            return false;
        return parts != null;
    }

    synchronized public void reset() {
        setDownloaded(0);
        if (parts != null) {
            for (Part p : parts) {
                p.setDownloaded(0);
                p.setState(DownloadInfo.Part.States.QUEUED);
            }
        }
    }

    /**
     * for multi part download, call every time when we need to know total download progress
     */
    synchronized public void calculate() {
        long total = 0;
        for (Part p : getParts())
            total += p.getDownloaded();
        setDownloaded(total);
    }

    synchronized public List<Part> getParts() {
        return parts;
    }

    synchronized public long getPartLength() {
        return partLength;
    }

    synchronized public void enableMultipart() {
        enableMultipart(PART_LENGTH);
    }

    synchronized public void enableMultipart(long partLength) {
        if (empty())
            throw new RuntimeException("Empty Download info, cant set multipart");

        if (!getRange())
            throw new RuntimeException("Server does not support RANGE, cant set multipart");

        this.partLength = partLength;

        long len = getLength();
        long last = len - 1;
        long count = len / partLength;
        if (len % partLength > 0)
            count++;

        if (count > 2) { // we only enable multipart for 3 and more parts
            parts = new ArrayList<Part>();
            long start = 0;
            for (int i = 0; i < count; i++) {
                long end = start + partLength - 1;
                if (end > last)
                    end = last;
                Part part = new Part();
                part.setIndex(i);
                part.setStart(start);
                part.setEnd(end);
                part.setState(DownloadInfo.Part.States.QUEUED);
                parts.add(part);
                start = end + 1;
            }
        }
    }

    /**
     * Check if we can continue download a file from new source. Check if new source has the same file length, title.
     * and supports for range
     * 
     * @param oldInfo
     *            old info source
     * @return true - possible to resume from new location
     */
    synchronized public boolean canResume(DownloadInfo oldInfo) {
        if (!oldInfo.getRange())
            return false;

        if (oldInfo.getContentFilename() != null && this.getContentFilename() != null) {
            if (!oldInfo.getContentFilename().equals(this.getContentFilename()))
                // one source has different name
                return false;
        } else if (oldInfo.getContentFilename() != null || this.getContentFilename() != null) {
            // one source has a have old is not
            return false;
        }

        if (oldInfo.getLength() != null && this.getLength() != null) {
            if (!oldInfo.getLength().equals(this.getLength()))
                // one source has different length
                return false;
        } else if (oldInfo.getLength() != null || this.getLength() != null) {
            // one source has length, other is not
            return false;
        }

        if (oldInfo.getContentType() != null && this.getContentType() != null) {
            if (!oldInfo.getContentType().equals(this.getContentType()))
                // one source has different getContentType
                return false;
        } else if (oldInfo.getContentType() != null || this.getContentType() != null) {
            // one source has a have old is not
            return false;
        }

        return true;
    }

    /**
     * copy resume data from oldSource;
     */
    synchronized public void resume(DownloadInfo oldSource) {
        super.resume(oldSource);
        parts = new ArrayList<Part>();
        for (int i = 0; i < oldSource.parts.size(); i++)
            parts.add(new Part(parts.get(i)));
        partLength = oldSource.partLength;
        downloaded = oldSource.downloaded;
    }

    public long getDownloaded() {
        return downloaded;
    }

    public void setDownloaded(long downloaded) {
        this.downloaded = downloaded;
    }

    @Override
    public void extract(final AtomicBoolean stop, final Runnable notify) {
        super.extract(stop, notify);
    }

    public JSONObject save() {
        JSONObject obj = super.save();
        JSONArray parts = new JSONArray();
        if (this.parts != null) {
            for (Part p : this.parts)
                parts.put(p.save());
            obj.put("parts", parts);
            obj.put("partLength", partLength);
        }
        // obj.put("count", count); // runtime information
        return obj;
    }

    public void load(JSONObject o) {
        super.load(o);
        JSONArray a = o.optJSONArray("parts");
        if (a != null) {
            this.parts = new ArrayList<Part>();
            for (int i = 0; i < a.length(); i++) {
                JSONObject ao = a.getJSONObject(i);
                this.parts.add(new Part(ao));
            }
            partLength = o.getLong("partLength");
        }
        // count = o.getLong("count");
    }
}
